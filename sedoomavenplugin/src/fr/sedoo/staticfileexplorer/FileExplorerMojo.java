package fr.sedoo.staticfileexplorer;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo (name="staticfileexplorer")
public class FileExplorerMojo extends AbstractMojo {

	 @Parameter( property = "staticfileexplorer.docroot")
	 private String docroot;
	
	 @Parameter( property = "staticfileexplorer.imagefolder")
	 private String imagefolder;

//	public final static String imagefolder="https://sourcesup.renater.fr/portailresif/images/";
	public final static String FOLDER_IMAGE="directory.png";
	public final static String PDF_IMAGE="pdf.png";
	public final static String ODT_IMAGE="odt.png";
	public final static String DOC_IMAGE="doc.png";
	public final static String UP_IMAGE="up.png";
	public final static String DEFAULT_IMAGE="file.png";
	public final static String LIST_FILE="list.html";
	public final static String ODT_EXTANSION=".odt";
	public final static String PDF_EXTANSION=".pdf";
	public final static String DOC_EXTANSION=".doc";
	
	 public void execute() throws MojoExecutionException
	    {
	        getLog().info( "Debut generation explorer statique" );
	        try
	        {
	        	addStaticExplorer(docroot);
	        }
	        catch (Exception e)
	        {
	        	throw new MojoExecutionException(e.getMessage());
	        }
	        getLog().info( "Fin generation explorer statique" );
	    }

	private void addStaticExplorer(String folderName) throws Exception 
	{
		StringBuilder aux = new StringBuilder();
		aux.append(getFileIntroduction());
		File current = new File(folderName);
		File[] content = current.listFiles();
		if (folderName.compareToIgnoreCase(getRoot()) != 0)
		{
			aux.append("<img src=\""+imagefolder+UP_IMAGE+"\" />&nbsp;");
			aux.append("<a href=\"../"+LIST_FILE+"\">..</a><br>");
		}
		for (int i = 0; i < content.length; i++) 
		{
			File file = content[i];
			if (file.isDirectory())
			{
				aux.append("<img src=\""+imagefolder+FOLDER_IMAGE+"\" />&nbsp;");
				aux.append("<a  href=\"./"+file.getName()+"/"+LIST_FILE+"\">"+file.getName()+"</a><br>");
				addStaticExplorer(file.getAbsolutePath());
			}
			else
			{
				if (file.getName().endsWith(LIST_FILE) == false)
				{
					if (file.getName().endsWith(ODT_EXTANSION))
					{
						aux.append("<img src=\""+imagefolder+ODT_IMAGE+"\" />&nbsp;");
					}
					else if (file.getName().endsWith(PDF_EXTANSION))
					{
						aux.append("<img src=\""+imagefolder+PDF_IMAGE+"\" />&nbsp;");
					}
					else if (file.getName().endsWith(DOC_EXTANSION))
					{
						aux.append("<img src=\""+imagefolder+DOC_IMAGE+"\" />&nbsp;");
					}
					else
					{
						aux.append("<img src=\""+imagefolder+DEFAULT_IMAGE+"\" />&nbsp;");
					}

					aux.append("<a target=\"_blank\" href=\"./"+file.getName()+"\">"+file.getName()+"</a><br>");
				}
			}
		}
		File list = new File(current, LIST_FILE);
		aux.append(getFileConclusion());
		FileUtils.writeStringToFile(list, aux.toString());
	}

	private String getFileIntroduction() 
	{
		return "<html><head><style>a{font-family:arial;font-size:14px;color:#08c;text-decoration:none;}a:hover{color:#005580;background-color: #eee;}</style></head><body>";
	}
	
	private String getFileConclusion()
	{
		return "<body></html>";
	}

	public String getRoot() {
		return docroot;
	}

	public void setRoot(String root) {
		this.docroot = root;
	}

	public String getImagefolder() {
		return imagefolder;
	}

	public void setImagefolder(String imagefolder) {
		this.imagefolder = imagefolder;
	}

}
